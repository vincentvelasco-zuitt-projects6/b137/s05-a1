package b137.velasco.s05a1;

import java.util.ArrayList;

public class Phonebook extends Contact{

    private ArrayList<String> contacts = new ArrayList<String>();


    //Constructor
    public Phonebook(){
        super();
    }

    public Phonebook(String name, String numbers, String addresses ){

        super(name, numbers, addresses);


    }

    // Getter
    public ArrayList<String> getContacts(){
        return contacts;

    }

    public void setContacts(String newContacts){
        this.contacts.add(newContacts);
    }


    public void showContact(){
        super.showContact();
    }


    public void showAdress(){
        super.showAddress();
    }


}
