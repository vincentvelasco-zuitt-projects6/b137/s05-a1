package b137.velasco.s05a1;

import java.util.ArrayList;

public class Contact {

    private String name;
    private ArrayList<String> numbers = new ArrayList<String>();
    private ArrayList<String> addresses = new ArrayList<String>();


    //Constructor
    public Contact(){

    }


    public Contact(String name, String numbers, String addresses){

        this.name = name;
        this.numbers.add(numbers);
        this.addresses.add(addresses);

    }


    // Getter
    public String getName(){
        return name;
    }

    public ArrayList<String> getNumbers(){
        return numbers;
    }

    public ArrayList<String> getAddreses(){
        return addresses;
    }


    // Setter
    public void setName(String newName){
        this.name = newName;
    }

    public void setNumbers(String newNumbers){
        this.numbers.add(newNumbers);
    }



    public void setAddress(String newAddress){
        this.addresses.add(newAddress);
    }


    public void showContact(){
        if(this.numbers != null){

            System.out.println(this.name + " has the following registered numbers: " + this.numbers);
        } else {

            System.out.println("No contacts");
        }


    }

    public void showAddress(){
        if(this.numbers != null){

            System.out.println(this.name + " has the following registered adresses: " + this.addresses);
        } else {

            System.out.println("No address");
        }







    }

}
